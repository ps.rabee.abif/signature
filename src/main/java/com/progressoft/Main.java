package com.progressoft;

import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.util.Arrays;
import java.util.Base64;

public class Main {
    public static void main(String[] args) throws Exception {
        KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("RSA");
        keyPairGen.initialize(2048);
        KeyPair pair = keyPairGen.generateKeyPair();
        PrivateKey privateKey = pair.getPrivate();//used by the client to sign the request
        PublicKey publicKey = pair.getPublic();//used by the server to verify the signing
        String message = "message to be signed";

        //****************** Start of Client signing
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] messageHash = md.digest(message.getBytes(StandardCharsets.UTF_8));

        Cipher clientCipher = Cipher.getInstance("RSA");
        clientCipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] digitalSignature = clientCipher.doFinal(messageHash);
        String encodedEncryption = new String(Base64.getEncoder().encode(digitalSignature));
        //****************** End of Client signing

        //****************** Start of Server verification
        Cipher serverCipher = Cipher.getInstance("RSA");
        serverCipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] decryptedMessageHash = serverCipher.doFinal(Base64.getDecoder().decode(encodedEncryption));

        byte[] newMessageHash = md.digest(message.getBytes(StandardCharsets.UTF_8));
        System.out.println(Arrays.equals(decryptedMessageHash, newMessageHash));
        //****************** End of Server verification
    }
}
